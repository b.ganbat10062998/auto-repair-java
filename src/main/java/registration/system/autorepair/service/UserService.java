package registration.system.autorepair.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import registration.system.autorepair.dto.LoginDto;
import registration.system.autorepair.dto.MainResponse;
import registration.system.autorepair.dto.UsersDto;
import registration.system.autorepair.entity.EStaff;
import registration.system.autorepair.entity.EUsers;
import registration.system.autorepair.repository.RStaff;
import registration.system.autorepair.repository.RUsers;
import registration.system.autorepair.utils.BCrypt;
import registration.system.autorepair.utils.CoreUtils;
import registration.system.autorepair.utils.MainResponseCode;

import java.time.LocalDateTime;
import java.util.Optional;

import static registration.system.autorepair.utils.ConstLocal.IS_ACTIVE_1;
import static registration.system.autorepair.utils.MainResponseCode.DUPLICATED_USERNAME_CODE;
import static registration.system.autorepair.utils.MainResponseCode.DUPLICATED_USERNAME_STR;
import static registration.system.autorepair.utils.MainResponseCode.SUCCESS_CODE;
import static registration.system.autorepair.utils.MainResponseCode.SUCCESS_STR;
import static registration.system.autorepair.utils.MainResponseCode.WRONG_EMAIL_STR;

@Service
@RequiredArgsConstructor
@Log4j2
public class UserService {
    public static final int WORKLOAD = 11;
    private final RStaff staffRepository;
    private final RUsers usersRepository;

    public ResponseEntity<Object> loginUser(final LoginDto request) {
        return null;
    }

    public ResponseEntity<Object> createUser(final UsersDto usersDto) {
        Optional<EStaff> checkStaff = this.staffRepository.findByRegisterNumberAndActive(usersDto.getRegister(), IS_ACTIVE_1);
        if (!CoreUtils.isValidEmail(usersDto.getEmail())) {
            return new ResponseEntity<>(new MainResponse(MainResponseCode.WRONG_EMAIL_CODE, WRONG_EMAIL_STR), HttpStatus.BAD_REQUEST);
        }
        if (checkStaff.isPresent()) {
            log.info("users info");
            return new ResponseEntity<>(new MainResponse(MainResponseCode.DUPLICATED_STAFF_CODE, MainResponseCode.DUPLICATED_STAFF_STR), HttpStatus.BAD_REQUEST);
        }
        if (!CoreUtils.checkPattern(usersDto.getPhoneNumber(), "^[0-9]{8}$")) {
            return new ResponseEntity<>(new MainResponse(MainResponseCode.WRONG_PHONE_CODE, MainResponseCode.WRONG_PHONE_STR), HttpStatus.BAD_REQUEST);
        }
        Long staffId = createStaff(usersDto.getRegister(), usersDto.getPhoneNumber(), usersDto.getLastName(), usersDto.getFirstName(), usersDto.getPositionId(), usersDto.getAddress(), usersDto.getIsContract());
        if (Boolean.TRUE.equals(usersDto.getIsAdmin())) {
            Optional<EUsers> checkUser = this.usersRepository.findByUsernameAndIsActive(usersDto.getUsername(), IS_ACTIVE_1);
            if (checkUser.isPresent()) {
                return new ResponseEntity<>(new MainResponse(DUPLICATED_USERNAME_CODE, DUPLICATED_USERNAME_STR), HttpStatus.BAD_REQUEST);
            }
            createAdmin(usersDto.getUsername(), usersDto.getPassword(), usersDto.getRoleId(), usersDto.getEmail(), staffId);
        }
        return new ResponseEntity<>(new MainResponse(SUCCESS_CODE, SUCCESS_STR), HttpStatus.OK);
    }

    private Long createStaff(final String register, final String mobileNumber, final String lastName, final String firstName, final Long positionId, final String address, final Short isContract) {
        EStaff staff = new EStaff();
        staff.setActive(IS_ACTIVE_1);
        staff.setCreatedDate(LocalDateTime.now());
        staff.setAddress(address);
        staff.setFirstName(firstName);
        staff.setIsContract(isContract);
        staff.setMobileNumber(mobileNumber);
        staff.setLastName(lastName);
        staff.setRegisterNumber(register);
        staff.setPositionId(positionId);
        this.staffRepository.save(staff);
        return staff.getId();
    }

    private void createAdmin(final String username, final String password, final Long roleId, final String email, final Long staffId) {
        EUsers user = new EUsers();
        user.setCreatedDate(LocalDateTime.now());
        user.setUsername(username);
        user.setEmail(email);
        user.setStaffId(staffId);
        user.setRoleId(roleId);
        user.setIsActive(IS_ACTIVE_1);
        user.setPassword(this.passwordHash(password));
        this.usersRepository.save(user);
    }

//    public ResponseEntity<Object> changePassword(final Long userId, final String oldPass, final String newPassword) {
//
//    }

    public String passwordHash(String password) {
        return BCrypt.generateHash(password, WORKLOAD);
    }
}
