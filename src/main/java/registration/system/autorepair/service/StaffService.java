package registration.system.autorepair.service;

import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import registration.system.autorepair.dto.StaffListDto;
import registration.system.autorepair.entity.EStaff;
import registration.system.autorepair.repository.StaffRepositoryNative;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StaffService {
    private final StaffRepositoryNative staffNativeRepo;

    public ResponseEntity<?> getStaffList() {
        List<StaffListDto> list = this.staffNativeRepo.getStaffList();
        if (list.isEmpty()) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

}
