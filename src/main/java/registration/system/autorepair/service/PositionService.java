package registration.system.autorepair.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import registration.system.autorepair.dto.MainResponse;
import registration.system.autorepair.dto.PositionDto;
import registration.system.autorepair.entity.EPositionInfo;
import registration.system.autorepair.repository.RPositionInfo;

import java.util.Optional;

import static registration.system.autorepair.utils.ConstLocal.IS_ACTIVE_1;
import static registration.system.autorepair.utils.MainResponseCode.DUPLICATED_CODE;
import static registration.system.autorepair.utils.MainResponseCode.DUPLICATED_STR;
import static registration.system.autorepair.utils.MainResponseCode.SUCCESS_CODE;
import static registration.system.autorepair.utils.MainResponseCode.SUCCESS_STR;

@Service
@RequiredArgsConstructor
@Component
@Log4j2
public class PositionService {
    private final RPositionInfo positionRepository;

    public ResponseEntity<?> createPosition(final PositionDto request) {
        Optional<EPositionInfo> positionOpt = this.positionRepository.findByPositionInfoAndActive(request.getPositionName(), IS_ACTIVE_1);
        if (positionOpt.isPresent()) {
            return new ResponseEntity<>(new MainResponse(DUPLICATED_CODE, DUPLICATED_STR), HttpStatus.BAD_REQUEST);
        }
        EPositionInfo position = new EPositionInfo();
        position.setPositionInfo(request.getPositionName());
        position.setActive(IS_ACTIVE_1);
        position.setSalary(request.getSalary());
        this.positionRepository.save(position);
        return new ResponseEntity<>(new MainResponse(SUCCESS_CODE, SUCCESS_STR), HttpStatus.OK);
    }

}
