package registration.system.autorepair.utils;

public class MainResponseCode {
    public static final int SUCCESS_CODE = 1;
    public static final String SUCCESS_STR = "Амжилттай бүртгэгдлээ.";
    public static final String UPDATE_SUCCESS_STR = "Амжилттай засварлалаа.";
    public static final String DELETE_SUCCESS_STR = "Амжилттай устгалаа.";
    public static final String DUPLICATED_STAFF_STR = "Регистр давхцаж байна.";
    public static final int DUPLICATED_STAFF_CODE = 1001;
    public static final String WRONG_EMAIL_STR = "Email хаяг буруу байна.";
    public static final int WRONG_EMAIL_CODE = 1002;
    public static final String WRONG_PHONE_STR = "Утасны дугаар буруу байна.";
    public static final int WRONG_PHONE_CODE = 1003;
    public static final String NOT_FOUND_PROD_TYPE_STR = "Бүтээгдэхүүний төрөл бүртгэлгүй байна.";
    public static final int NOT_FOUND_PROD_TYPE_CODE = 1004;
    public static final String WRONG_TYPE_STR = "\"Бүтээгдэхүүн тухайн төрөлд бүртгэлтэй байна.\"";
    public static final int WRONG_TYPE_CODE = 1005;
    public static final int PRODUCT_NOT_FOUND_CODE = 1006;
    public static final String PRODUCT_NOT_FOUND_STR = "\"Бүтээгдэхүүн мэдээлэл олдсонгүй.\"";
    public static final String DUPLICATED_USERNAME_STR = "Хэрэглэгчийн нэвтрэх нэр давхцаж байна.";
    public static final int DUPLICATED_USERNAME_CODE = 1007;
    public static final int EXISTED_TYPE_CODE = 1008;
    public static final String EXISTED_TYPE_STR = "Бүтээгдэхүүний төрөл бүртгэлтэй байна.";
    public static final int NOT_FOUND_CODE = 400;
    public static final String NOT_FOUND_STR = "Үр дүн олдсонгүй";
    public static final int DUPLICATED_CODE = 1009;
    public static final String DUPLICATED_STR = "Давхардаж байна.";


}
