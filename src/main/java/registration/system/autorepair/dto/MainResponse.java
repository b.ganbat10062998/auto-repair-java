package registration.system.autorepair.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MainResponse {
    private int code;
    private Long value;
    private String message;

    public MainResponse(int code, Long value, String message) {
        this.code = code;
        this.value = value;
        this.message = message;
    }

    public MainResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
