package registration.system.autorepair.dto;

import lombok.Getter;
import lombok.Setter;
import registration.system.autorepair.enums.EStaffGender;

@Getter
@Setter
public class UsersDto {
    private String username;
    private String register;
    private Boolean isAdmin;
    private String firstName;
    private String lastName;
    private EStaffGender sex;
    private Long positionId;
    private String email;
    private String phoneNumber;
    private String password;
    private String address;
    private Short isContract;
    private Long roleId;
}
