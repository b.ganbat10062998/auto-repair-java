package registration.system.autorepair.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProductListDto {
    private Long id;
    private String productName;
    private Short active;
    private Long quantity;
    private String createdDate;
    private String updatedDate;
    private String image;
    private String description;
    private String typeName;
}
