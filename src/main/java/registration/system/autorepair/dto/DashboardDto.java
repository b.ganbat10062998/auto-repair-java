package registration.system.autorepair.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class DashboardDto {
    private BigDecimal product;
    private BigDecimal order;
    private BigDecimal total;
    private BigDecimal staff;
}
