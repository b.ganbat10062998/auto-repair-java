package registration.system.autorepair.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProductRequestDto {
    private String name;
    private Long typeId;
    private BigDecimal quantity;
    private String imageUrl;
    private String description;
    private Long productId;
}
