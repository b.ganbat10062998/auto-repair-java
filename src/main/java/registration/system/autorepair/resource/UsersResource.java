package registration.system.autorepair.resource;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import registration.system.autorepair.dto.UsersDto;
import registration.system.autorepair.service.StaffService;
import registration.system.autorepair.service.UserService;

@RestController
@RequestMapping("staff")
@RequiredArgsConstructor
public class UsersResource {
    private final StaffService staffService;
    private final UserService userService;

    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @PostMapping
    public ResponseEntity<Object> createStaff(@RequestBody UsersDto request) {
        return this.userService.createUser(request);
    }
}
