package registration.system.autorepair.resource;

import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import registration.system.autorepair.dto.ProductTypeDto;
import registration.system.autorepair.service.ProductService;

@RestController
@RequestMapping("product-type")
@RequiredArgsConstructor
public class ProductTypeResource {
    private final ProductService productService;

    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @PostMapping
    public ResponseEntity<?> createProductType(@RequestBody ProductTypeDto request) {
        return productService.createProductType(request);
    }
    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @GetMapping
    public ResponseEntity<?> deleteProductType(@RequestParam("id") Long id) {
        return this.productService.deleteType(id);
    }
    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @GetMapping("list")
    public ResponseEntity<?> getTypeList() {
        return this.productService.getProductTypeList();
    }
}
