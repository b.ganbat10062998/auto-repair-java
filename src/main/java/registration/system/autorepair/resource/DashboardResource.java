package registration.system.autorepair.resource;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import registration.system.autorepair.service.DashboardService;

@RestController
@RequiredArgsConstructor
@RequestMapping("dashboard")
public class DashboardResource {
    private final DashboardService dashboardService;

    @GetMapping
    public ResponseEntity<?> getDashboardData(){
        return this.dashboardService.getDashboardData();
    }
}
