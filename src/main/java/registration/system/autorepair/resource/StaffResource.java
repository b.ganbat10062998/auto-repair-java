package registration.system.autorepair.resource;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import registration.system.autorepair.service.StaffService;

@RestController
@RequestMapping("staff")
@RequiredArgsConstructor
public class StaffResource {
    private final StaffService staffService;

    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @GetMapping("list")
    public ResponseEntity<?> getStaffList() {
        return this.staffService.getStaffList();
    }
}
