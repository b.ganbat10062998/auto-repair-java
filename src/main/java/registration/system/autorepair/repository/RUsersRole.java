package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entity.EUsersRole;

@Repository
public interface RUsersRole extends JpaRepository<EUsersRole, Long> {
}
