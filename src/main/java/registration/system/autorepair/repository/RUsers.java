package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entity.EUsers;

import java.util.Optional;

@Repository
public interface RUsers extends JpaRepository<EUsers, Long> {

    Optional<EUsers> findByUsernameAndIsActive(String username, Short isActive);
}
