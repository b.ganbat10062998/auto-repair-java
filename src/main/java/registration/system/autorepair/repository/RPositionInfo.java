package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entity.EPositionInfo;

import java.util.Optional;

@Repository
public interface RPositionInfo extends JpaRepository<EPositionInfo, Long> {
    Optional<EPositionInfo> findByPositionInfoAndActive(String name, Short active);
}
