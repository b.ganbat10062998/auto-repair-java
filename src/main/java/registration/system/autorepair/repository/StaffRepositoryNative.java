package registration.system.autorepair.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.dto.ProductListDto;
import registration.system.autorepair.dto.StaffListDto;

import java.util.List;
@Repository
public class StaffRepositoryNative {
    @PersistenceContext
    private EntityManager em;

    public List<StaffListDto> getStaffList() {
        return em.createNativeQuery("select\n" +
                        "\ts.last_name as \"lastName\",\n" +
                        "\ts.first_name as \"firstName\",\n" +
                        "\ts.register as \"register\" ,\n" +
                        "\ts.mobile_number as \"mobileNumber\",\n" +
                        "\ts.address as \"address\" ,\n" +
                        "\tpi2.position_name as \"positionName\",\n" +
                        "\tpi2.salary as \"salary\",\n" +
                        "\tu.username as \"username\",\n" +
                        "\tcase \n" +
                        "\t\twhen u.username is not null then 'Админ'\n" +
                        "\t\telse 'Энгийн'\n" +
                        "\tend as \"isAdmin\",\n" +
                        "\tcase \n" +
                        "\t\twhen s.is_contract = 1 then 'Гэрээ байгуулсан'\n" +
                        "\t\telse 'Гэрээ байгуулаагүй'\n" +
                        "\tend\t as \"contract\",\n" +
                        "\tto_char(s.created_date ,\n" +
                        "\t'YYYY-mm-dd') as \"createdDate\"\n" +
                        "from\n" +
                        "\tstaff s\n" +
                        "inner join position_info pi2 on\n" +
                        "\tpi2.id = s.position_id\n" +
                        "left join users u on\n" +
                        "\tu.staff_id = s.id\n" +
                        "where\n" +
                        "\ts.active = 1").unwrap(NativeQuery.class)
                .setResultTransformer(Transformers.aliasToBean(StaffListDto.class))
                .getResultList();
    }

//    public
}
