package registration.system.autorepair.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.dto.ProductListDto;

import java.util.List;

@Repository
public class ProductRepositoryNative {

    @PersistenceContext
    private EntityManager em;

    public List<ProductListDto> getProductList() {
        return em.createNativeQuery("select\n" +
                        "p.id as \"id\",\n" +
                        "p.\"name\" as \"productName\",\n" +
                        "p.active as \"active\",\n" +
                        "p.quantity as \"quantity\",\n" +
                        "to_char(p.created_date,'YYYY-mm-dd HH:mi:ss') as \"createdDate\",\n" +
                        "to_char(p.updated_date ,'YYYY-mm-dd HH:mi:ss') as \"updatedDate\",\n" +
                        "p.image as \"image\",\n" +
                        "p.description as \"description\",\n" +
                        "pt.\"name\" as \"typeName\"\n" +
                        "from\n" +
                        "product p\n" +
                        "inner join product_type pt on\n" +
                        "pt.id = p.type_id").unwrap(NativeQuery.class)
                .setResultTransformer(Transformers.aliasToBean(ProductListDto.class))
                .getResultList();
    }
}
