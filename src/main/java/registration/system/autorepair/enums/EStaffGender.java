package registration.system.autorepair.enums;

import lombok.Getter;

@Getter
public enum EStaffGender {
    MALE("male"),
    FEMALE("female");
    private final String value;

    EStaffGender(String value) {
        this.value = value;
    }
}
